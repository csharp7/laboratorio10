﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Laboratorio10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Pessoa> pessoas = new List<Pessoa> 
            {
                new Pessoa{Nome="Ana", DataNascimento=new DateTime(1980,3,14), Casada=true},
                new Pessoa{Nome="Paulo", DataNascimento=new DateTime(1978,10,23), Casada=true},
                new Pessoa{Nome="Maria", DataNascimento=new DateTime(2000,1,10), Casada=false},
                new Pessoa{Nome="Carlos", DataNascimento=new DateTime(1999,12,11), Casada=false},
            };

            var linq1 = from p in pessoas
                        where p.Casada && p.DataNascimento >= new DateTime(1980, 1, 1)
                        select p;

            foreach (var pessoa in linq1)
            {
                Console.WriteLine(pessoa);
            }

            var linq2 = pessoas.Where(p => p.Casada && p.DataNascimento >= new DateTime(1980, 1, 1));

            foreach (var pessoa in linq2)
            {
                Console.WriteLine(pessoa);
            }

            var linqCasadas = pessoas.Where(p => p.Casada);
            var linqSolteiras = pessoas.Where(p => !p.Casada);
            int numCasadas = linqCasadas.Count();
            int numSolteiras = linqSolteiras.Count();

            Console.WriteLine($"Pessoas Casadas = {numCasadas}");
            foreach (var pessoa in linqCasadas)
            {
                Console.WriteLine(pessoa);
            }

            Console.WriteLine($"Pessoas Solteiras = {numSolteiras}");
            foreach (var pessoa in linqSolteiras)
            {
                Console.WriteLine(pessoa);
            }

            var maisVelha = from p in pessoas
                            where p.DataNascimento == pessoas.Min(p => p.DataNascimento)
                            select p;
            foreach (var pessoa in maisVelha)
            {
                Console.WriteLine($"Pessoa mais velha = {pessoa}");
            }

            var SolteiraMaisNova = from p in pessoas
                where p.DataNascimento == pessoas.Max(p => p.DataNascimento) && p.Casada == false
                select p;
            foreach (var pessoa in SolteiraMaisNova)
            {
                Console.WriteLine($"Pessoa solteira mais nova = {pessoa}");
            }
        }
    }
}
